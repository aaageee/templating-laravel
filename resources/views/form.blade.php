<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form>
        <label for="first_name">First Name:</label>
        <br>
        <input type="text" id="first_name">
        <br>
        <label for="last_name">Last Name:</label>
        <br>
        <input type="text" id="last_name">
        <br>
        <label>Gender:</label>
        <br>
        <input type="radio" name="gender">Male
        <br>
        <input type="radio" name="gender">Female
        <br>
        <input type="radio" name="gender">Other
        <br>
        <br>
        <label>Nationality</label>
        <br>
        <select>
            <option>Indonesia</option>
            <option>American</option>
            <option>British</option>
        </select>
        <br>
        <label>Language Spoken:</label>
        <br>
        <input type="checkbox">Bahasa Indonesia
        <br>
        <input type="checkbox">English
        <br>
        <input type="checkbox">Other       
        <br>
        <br>
        <label>Bio:</label>
        <br>
        <textarea>
            </textarea>
        <br>

    </form>
    <form action="/welcome">
        <input type="submit" value="Sign Up">

    </form>
   

</body>
</html>